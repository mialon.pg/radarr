#!/bin/sh
set -e

export CFG_DIR="${CFG_DIG:-/config}"

gen-config.sh

exec "$@"
