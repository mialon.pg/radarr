FROM alpine:3.21.3

# renovate: datasource=github-releases depName=Radarr/Radarr versioning=loose
ENV RADARR_VER=5.19.3.9730
ARG RADARR_BRANCH=master

WORKDIR /radarr

COPY --chmod=755 *.sh /usr/local/bin/

RUN apk add --no-cache \
        tini \
        icu-libs \
        libintl \
        libmediainfo \
        sqlite-libs \
        xmlstarlet \
        mono --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/ \
 && test "$(uname -m)" = aarch64 && ARCH=arm64 || ARCH=x64 \
 && wget -O- https://github.com/Radarr/Radarr/releases/download/v${RADARR_VER}/Radarr.${RADARR_BRANCH}.${RADARR_VER}.linux-musl-core-${ARCH}.tar.gz \
        | tar xz --strip-components=1 \
 && rm -rf Radarr.Update

ENV XDG_CONFIG_HOME=/config

ENTRYPOINT ["/sbin/tini", "--", "/usr/local/bin/entrypoint.sh"]
CMD ["/radarr/Radarr", "--no-browser", "--data=/config"]
